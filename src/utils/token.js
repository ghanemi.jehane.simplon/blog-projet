
import fs from 'fs';
import jwt from 'jsonwebtoken';
import passport from 'passport';

import { ExtractJwt, Strategy } from 'passport-jwt';
import { UserRepository } from '../repository/userRepository';


const privateKey = fs.readFileSync('config/id_rsa');
const publicKey = fs.readFileSync('config/id_rsa.pub');

/**
 * Fonction qui génère un JWT en se basant sur la clef privée.
 * Il est actuellement configuré pour expiré au bout de 1H (60*60 secondes) mais ça peut être modifié
 * @param {object} payload le body qui sera mis dans le token
 * @returns le JWT
 */
export function generateToken(payload) {
    const token = jwt.sign(payload, privateKey,{algorithm: 'RS256', expiresIn: 60*60 });
    return token;
}

/**
 * Fonction qui configure la stratégie JWT de passport, il va chercher le token dans les headers de la 
 * requête en mode {"authorization":"Bearer leToken"}
 */
export function configurePassport() {
    passport.use(new Strategy({
        jwtFromRequest: ExtractJwt.fromAuthHeaderAsBearerToken(),
        secretOrKey: publicKey,
        algorithms: ['RS256']
    }, async (payload, done) => {
        //Si on arrive ici, c'est que le token est valide et a été décodé, on se
        //retrouve avec le body du token (payload) et on doit ce servir de celui ci
        //pour récupérer le User correspondant à ce token, on utilise ici la méthode
        //findByEmail pour récupérer le User et pouvoir y donner accès dans les routes protégées
        try {
            const user = await UserRepository.findByEmail(payload.email);
            
            if(user) {
               
                return done(null, user);
            }
            
            return done(null, false);
        } catch (error) {
            console.log(error);
            return done(error, false);
        }
    }))

}