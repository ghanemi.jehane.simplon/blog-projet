import { connection } from "./connection";
import {User} from '../entity/User';

export class UserRepository{

    // FIND BY ID
    static async findById(id){
        const [rows] = await connection.execute('SELECT * FROM user WHERE id=?', [id]);
    if (rows.length === 1) {
        return new User(rows[0].email, rows[0].password, rows[0].role,rows[0].id)
    }
    return null;

    }
    // FIND BY EMAIL

    static async findByEmail(email){
        const [rows] = await connection.execute('SELECT * FROM user WHERE email=?', [email]);
    if (rows.length === 1) {
        return new User(rows[0].email, rows[0].password, rows[0].role, rows[0].id)
    }
    return null;

    }
    //ADD USER
    static async addUser(user){
        const [rows] = await connection.execute('INSERT INTO user (email, password, role) VALUES (?,?,?)', [user.email, user.password, user.role]);
        user.id= rows.insertId; 
    }

      // UPDATE

      static async updateUser(user){
        const[rows] = await connection.query('UPDATE user SET email=?, password=?, role=? WHERE id=?', [user.email, user.password, user.role, user.id]);
    }

      // DELETE
      static async deleteUser(id){
        const [rows] = await connection.execute('DELETE FROM user WHERE id=?', [id]);

    }
}