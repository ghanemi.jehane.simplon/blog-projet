import { connection } from "./connection";
import {Article} from '../entity/Article';
import { User } from "../entity/User";

export class ArticleRepository{

    //FIND ALL ARTICLES
    static async findAll(){
        const [rows] = await connection.execute('SELECT *, article.id AS articleid FROM article INNER JOIN user ON user_id = user.id');

        const articles = [];
        for(const row of rows){
            let instance = new Article(row.title, row.description, row.date, row.auteur, row.image,row.articleid, row.user_idd);
            instance.user = new User(row.email, row.password, row.role, row.user_id );
            articles.push(instance);
        }
        return articles;
    }
    //FIND BY ID
    static async findById(id){
        const [rows] = await connection.execute('SELECT * FROM article WHERE id=?', [id]);
    if (rows.length === 1) {
        return new Article(rows[0].title, rows[0].description, rows[0].date, rows[0].auteur, rows[0].image, rows[0].user_id, rows[0].id)
    }
    return null;

    }
    // ADD

    static async addArticle(article){
        const [rows] = await connection.execute
        ('INSERT INTO article (title, description, `date`, auteur,image, user_id) VALUES (?,?,?,?,?,?)', 
        [article.title, article.description, article.date, article.auteur, article.image , article.user_id]);
     article.id= rows.insertId; 
    }
    
    // UPDATE

    static async updateArticle(article){
        const[rows] = await connection.query('UPDATE article SET title=?, description=?, date=?, auteur=?, image=? WHERE id=?', [article.title, article.description, article.date,article.auteur, article.image, article.id]);
    }
    // DELETE
    static async deleteArticle(id){
        const [rows] = await connection.execute('DELETE FROM article WHERE id=?', [id]);

    }
}