export class User{
    id;
    email;
    password;
    role;

    constructor(email, password, role="user", id=null){
        this.email= email;
        this.password= password;
        this.role= role;
        this.id= id;
    }
}