export class Article{
    id;
    title;
    description;
    date;
    auteur;
    image;
    user_id;
    user;

    constructor(title, description, date, auteur, image, id, user_id){
        this.title= title;
        this.description= description;
        this.date= date;
        this.auteur= auteur;
        this.image= image;
        this.id = id;
        this.user_id= user_id;
        
    }
}