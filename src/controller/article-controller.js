import {Router} from "express";
import passport from "passport";
import { ArticleRepository } from "../repository/articleRepository";

export const articleController = Router();

//FIND ALL ARTICLES
articleController.get('/', async (req, resp) => {
    let article = await ArticleRepository.findAll();
    resp.json(article);
    });


//FIND BY ID
articleController.get('/:id', async (req, resp) =>{
    let user = await ArticleRepository.findById(req.params.id)
    if(!user){
        resp.status(404).json({error: 'Not found'});
        return;
    }
    
    resp.json(user)
})

// ADD ARTICLE


articleController.post('/', async(req, resp) =>{
   await ArticleRepository.addArticle(req.body);
    resp.status(201).json(req.body);
    })

// DELETE ARTICLE

articleController.delete('/:id', async (req,resp)=>{
    try{
      const response =  await ArticleRepository.deleteArticle(req.params.id);
       
        resp.status(204).end()
        
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }

})

// UPDATE ARTICLE

articleController.patch('/:id', async (req, resp) =>{
    await ArticleRepository.updateArticle(req.body);
    resp.end();
})