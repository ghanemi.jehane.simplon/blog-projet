import { Router} from "express";
import { User } from "../entity/User";
import { UserRepository } from "../repository/userRepository";
import bcrypt from 'bcrypt';
import { generateToken } from "../utils/token";
import passport from "passport";


export const userController = Router();

userController.get('/account', passport.authenticate('jwt', {session:false}), (req, res) => {
    res.json(req.user);
});


//FIND BY ID
userController.get('/:id', async (req, resp) =>{
    let user = await UserRepository.findById(req.params.id)
    if(!user){
        resp.status(404).json({error: 'Not found'});
        return;
    }
    
    resp.json(user)
})


// ADD USER
userController.post('/', async (req, res) => {
    try {
        const newUser = new User();
        Object.assign(newUser, req.body);
        
        const exists = await UserRepository.findByEmail(newUser.email);
        if(exists) {
            res.status(400).json({error: 'Email already taken'});
            return;
        }
        //On assigne user en role pour pas qu'un user puisse choisir son rôle à l'inscription
        newUser.role = 'user';
        //On hash le mdp du user pour pas le stocker en clair
        newUser.password = await bcrypt.hash(newUser.password, 11);

        await UserRepository.addUser(newUser);
        res.status(201).json(newUser);

    } catch (error) {
        console.log(error);
        res.status(500).json(error);
    }
});



// DELETE USER

userController.delete('/:id',async (req,resp)=>{
    try{
        await UserRepository.deleteUser(req.params.id);
        resp.status(204).end()
    }catch (e){
        console.log(e)
        resp.status(500).end()
    }

})



//LOGIN
userController.post('/login', async (req,res) => {
    try{

        const user = await UserRepository.findByEmail(req.body.email);
        if(user) {
            const samePassword = await bcrypt.compare(req.body.password, user.password);
            if(samePassword) {
                res.json({
                    user,
                    token: generateToken({
                        email: user.email,
                        id: user.id,
                        role:user.role
                    })
                });
                return;
            }
        }
        res.status(401).json({error: 'Wrong email and/or password'});
    }catch(error) {
        console.log(error);
        res.status(500).json(error);
    }
});



// UPDATE 

userController.patch('/profile', passport.authenticate('jwt', {session:false}), async(req,res) => {
    try {
        let data = await UserRepository.findById(req.user.id);
        if (!data) {
            res.send(404).end();
            return
        }
        let update = { ...data, ...req.body };

        await UserRepository.updateUser(update);
        res.json(update);
    } catch (error) {
        console.log(error);
        res.status(400).end();
    }
})

