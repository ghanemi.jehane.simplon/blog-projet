DROP TABLE IF EXISTS article;

CREATE TABLE article (
        id INTEGER PRIMARY KEY AUTO_INCREMENT,
        title VARCHAR(255),
        description VARCHAR(255),
        date DATE,
        auteur VARCHAR(255),
        image VARCHAR(255),
        user_id INT(11)
);

DROP TABLE IF EXISTS user;
CREATE TABLE user(
    id INTEGER PRIMARY KEY AUTO_INCREMENT,
    email VARCHAR(255),
    password VARCHAR(255),
    role VARCHAR(255)
);